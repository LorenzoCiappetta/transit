#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <errno.h>
#include <sys/sem.h>
#include <unistd.h>

#define MAX_TRAIN_NUMBER 10000//10 * N

//just to avoid making more global variables
typedef struct train_args{ 
  //int id //train id could be passed by arguments instead of using mutex...
  int T;
  int TMin;
  int TMax;
}train_args;

//managed via mutex
int train_waiting = 0;
int* trains_on_tracks;
int train_passing = 0;
int train_passed = 0;
int id_counter = 0; 

pthread_mutex_t mutexes[3]; //{train_waiting, trains_on_tracks, id_counter}
int tracks_semaphore_id;

void initialize_mutexes(){
  int ret;
  int i;
  for(i = 0; i < 3; i++){
    ret = pthread_mutex_init(&mutexes[i], NULL);
    if(ret){
      errno = ret;
      printf("error: mutex_init()\n");
      _exit(1);
    }
  }
}

void destroy_mutexes(){
  int ret;
  int i;
  for(i = 0; i < 3; i++){
    ret = pthread_mutex_destroy(&mutexes[i]);
    if(ret){
      errno = ret;
      printf("error: mutex_destroy()\n");
      _exit(1);
    }
  }
}

void initialize_semaphore(int value){
  int ret;
  
  tracks_semaphore_id = semget(IPC_PRIVATE, 1, IPC_CREAT | 0600);
  if(-1 == tracks_semaphore_id){
    printf("error: semget()\n");
    _exit(1);
  }
  
  ret = semctl(tracks_semaphore_id, 0, SETVAL, value);
  if(-1 == ret){
    printf("error: semctl()\n");
    _exit(1);
  }  
}

void print_current_status(){
  printf("train_passed: %d, train_passing: %d, train_waiting: %d\n", train_passed, train_passing, train_waiting);
}

//this rapresents the train
void* train_handler(void *args){
  int ret;
  struct sembuf sem_op = {0, -1, 0};
  
  train_args ta = *(train_args*)args;
  int T = ta.T;
  int TMin = ta.TMin;
  int TMax = ta.TMax;
  
  //fetch id via mutex
  int id;
  
  ret = pthread_mutex_lock(&mutexes[2]);
  if(ret){
      errno = ret;
      printf("error: mutex_lock()\n");
      _exit(1);  
  }
  
  id = id_counter;
  id_counter++;
  
  ret = pthread_mutex_unlock(&mutexes[2]);
  if(ret){
      errno = ret;
      printf("error: mutex_unlock()\n");
      _exit(1);  
  }
    
  //after train has arrived
  //add 1 to train_waiting via mutex and print status  
  ret = pthread_mutex_lock(&mutexes[0]);
  if(ret){
      errno = ret;
      printf("error: mutex_lock()\n");
      _exit(1);  
  }
  
  train_waiting++;
  print_current_status();
  
  ret = pthread_mutex_unlock(&mutexes[0]);
  if(ret){
      errno = ret;
      printf("error: mutex_unlock()\n");
      _exit(1);  
  } 
    
  //sem_wait
  ret = semop(tracks_semaphore_id, &sem_op, 1);
  if(-1 == ret){
    printf("error: semop()\n");
    _exit(1);
  }
  
  //CS: crossing the tracks
  //remove 1 to train_waiting via mutex
  ret = pthread_mutex_lock(&mutexes[0]);
  if(ret){
      errno = ret;
      printf("error: mutex_lock()\n");
      _exit(1);  
  }
  
  train_waiting--;
  
  ret = pthread_mutex_unlock(&mutexes[0]);
  if(ret){
      errno = ret;
      printf("error: mutex_unlock()\n");
      _exit(1);  
  }
  
  //add id to trains_on_track via mutex and print status
  ret = pthread_mutex_lock(&mutexes[1]);
  if(ret){
      errno = ret;
      printf("error: mutex_lock()\n");
      _exit(1);  
  }
  
  trains_on_tracks[id] = 1;
  train_passing++;
  print_current_status();
  
  ret = pthread_mutex_unlock(&mutexes[1]);
  if(ret){
      errno = ret;
      printf("error: mutex_unlock()\n");
      _exit(1);  
  }
  
  //sleep T
  usleep(T*1000);
  
  //remove id from trains_on_trak via mutex
  ret = pthread_mutex_lock(&mutexes[1]);
  if(ret){
      errno = ret;
      printf("error: mutex_lock()\n");
      _exit(1);  
  }
  
  trains_on_tracks[id] = 0;
  train_passing--;
  train_passed++;
  print_current_status();
  
  ret = pthread_mutex_unlock(&mutexes[1]);
  if(ret){
      errno = ret;
      printf("error: mutex_unlock()\n");
      _exit(1);  
  }
  //END CS
  //sem_post
  sem_op.sem_op = 1;
  ret = semop(tracks_semaphore_id, &sem_op, 1);
  if(-1 == ret){
    printf("error: sem_op()\n");
    _exit(1);
  }

  pthread_exit(NULL);
}

int main(int argc, char* argv[]){
  if(argc < 5){
    printf("error: too few parameters: N, T, TMin, TMax\n");
    return -1;
  }
  
  int ret;
  int N = atoi(argv[1]);
  int TMin = atoi(argv[3]);
  int TMax = atoi(argv[4]);
  
  initialize_mutexes();
  initialize_semaphore(N);
  
  train_args args;
  args.T = atoi(argv[2]);
  args.TMin = atoi(argv[3]);
  args.TMax = atoi(argv[4]);
  
  trains_on_tracks = calloc(MAX_TRAIN_NUMBER, sizeof(int));
  
  //starts generating trains
  int i;
  pthread_t train[MAX_TRAIN_NUMBER];
  for(i = 0; i < MAX_TRAIN_NUMBER; i++){ //infinite trains or N trains?
    
    ret = pthread_create(&train[i], NULL, train_handler, (void*)&args);
    if(ret){
      errno = ret;
      printf("error: pthread_create()\n");
      _exit(1);
    }
    
    //sleep between TMin, TMax before next train
    int time_to_sleep = (rand() % (TMax - TMin + 1)) + TMin; 
    usleep(time_to_sleep*1000);
    
  }
  
  for(i = 0; i < MAX_TRAIN_NUMBER; i++){ //infinite trains or N trains?
    
    ret = pthread_join(train[i], NULL);
    if(ret){
      errno = ret;
      printf("error: pthread_join()\n");
      _exit(1);
    }
    
  }
  destroy_mutexes();
  free(trains_on_tracks);
}